**README**


**The Models for Privacy Engineering Interest Group**

This interest group focuses on the sharing of practices for privacy engineering based on the use of  models:

To foster exchange of ideas and explanation. This is a universal practice, as pointed out by the national science teaching association, which has developed a practice program which leverages models as helpful tools for representing ideas and explanations. These tools include diagrams, drawings, physical replicas, mathematical representations, analogies, and computer simulations (https://ngss.nsta.org/Practices.aspx?id=2).

To support engineering. INCOSE defines model-based systems engineering (MBSE) as the formalized application of modeling to support system requirements, design, analysis, verification and validation activities beginning in the conceptual design phase and continuing throughout development and later life cycle phases (https://www.omgwiki.org/MBSE/doku.php).

You can read more https://projects.eclipse.org/interest-groups/models-privacy-engineering



**About Eclipse Interest Groups**

Eclipse Interest Groups are a light-weight association of Eclipse Foundation Members that share a common interest in a topic or domain (Participating Members) that inherit and rely upon the Eclipse Foundation’s overall governance, sufficient to enable the Participating Members to collaborate effectively.  All participants must comply with the agreements, policies and procedures of the Eclipse Foundation.  In particular and without limiting the foregoing, at all times participants in all Eclipse Interest Groups must conform to the Eclipse Foundation’s Intellectual Property and Antitrust Policies. You can read more about the requirements to participate and related governance in the formal Eclipse Interest Group Process via https://www.eclipse.org/org/collaborations/interest-groups/process.php.

**Participation**

In order to participate in an Interest Group, an organization must execute the Eclipse Foundation Membership Agreement as well as the Eclipse Foundation Member Committer and Contributor Agreement (if not already in place) and to follow any other steps on how to participate that are defined by the Eclipse Foundation.   

**Guiding Principles**


All Interest Groups must operate in an open and transparent manner.

Open - Everyone participates with the same rules; there are no rules to exclude any potential collaborators which include, of course, direct competitors in the marketplace.
Transparent - Minutes, plans, and other artifacts are open and easily accessible to all.

Interest Groups, like all communities associated with the Eclipse Foundation, must operate in adherence to the Eclipse Foundation’s Code of Conduct.

**Materials**

Interest Groups must produce agendas and minutes for all meetings. Interest Groups may, at their discretion, produce artifacts such as documents, whitepapers, architectures, blueprints, diagrams, presentations, and the like; however, Interest Groups must not develop software, software documentation, or specifications.

 
